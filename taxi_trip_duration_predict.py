
# coding: utf-8

# # 纽约出租车乘客行程时长预测
# ## 导入库

# In[1]:


import pandas as pd
import numpy as np
import holidays
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split


# ## 数据预处理
# ### 1.读入数据并查看

# In[2]:


# data = pd.read_csv("trip_data4.csv", skipinitialspace=True )
data = pd.read_excel("trip_data1.csv")


# In[3]:


data.head()


# In[4]:


data.shape


# In[5]:


data.describe().T


# ### 2.处理缺失值

# In[6]:


# 查看哪些列有空值.
data.isnull().sum().sort_values(ascending=False)


# In[7]:


# 由于store_and_fwd_flag缺失值太多，故去除该列
# data.drop('store_and_fwd_flag',axis=1,inplace=True)
data = data.drop(columns='store_and_fwd_flag')


# In[8]:


# 删除其他缺失数据的行
data.dropna(axis = 0, inplace=True)


# In[9]:


data.head()


# ### 3.处理异常值

# In[10]:


# 载客数大于6的为异常值，删除
data.drop(data[data['passenger_count']>6].index,axis=0, inplace=True)
data.shape


# In[11]:


#查看经纬度的最大、最小值发现，经纬度数据存在极端值。
#根据实际情况，经纬度范围限制在纬度（-90,90）之间，经度在（-180,180）之间。超过范围的，作删除处理.
data.drop(((data[data['pickup_latitude']<-90])|(data[data['pickup_latitude']>90]))
         .index,axis=0, inplace=True)
data.drop(((data[data['pickup_longitude']<-180])|(data[data['pickup_longitude']>180]))
         .index,axis=0, inplace=True)
data.drop(((data[data['dropoff_latitude']<-90])|(data[data['dropoff_latitude']>90]))
         .index,axis=0, inplace=True)
data.drop(((data[data['dropoff_longitude']<-180])|(data[data['dropoff_longitude']>180])
         ).index,axis=0, inplace=True)
data.shape


# ### 4.特征工程

# In[12]:


# 查看vendor_id值，为非数值型
print(data.vendor_id.unique())


# In[13]:


# 数值化vendor_id
data=pd.get_dummies(data,columns=['vendor_id'],drop_first=True)


# In[14]:


# 从上车日期中抽取数值特征
def get_datetime_segment(data):
    temp = pd.to_datetime(data['pickup_datetime'])
    data['month'] = temp.dt.month
    # 提取星期特征
    data['day'] = temp.dt.dayofweek
    # 提取时、分、秒特征
    data['hour'] = temp.dt.hour
    data['minute'] = temp.dt.minute
    data['second'] = temp.dt.second
    # 是否是假期
    l = temp.dt.date.values
    h = holidays.US(state="NY")
    holi = []
    for i in l:
        if i in h:
            holi.append(1)
        else:
            holi.append(0)
    data["holiday"] = holi    
    return data

data = get_datetime_segment(data)
data.head()


# In[15]:


# 删除无用的特征
data.drop(['medallion','hack_license','pickup_datetime', 'dropoff_datetime'],axis=1,inplace=True)
data.head()


# In[16]:


# 提取特征数据和标签数据
Y = data['trip_time_in_secs'].values /60.0     #转换成分钟
X = data.drop(['trip_time_in_secs'],axis=1)
X.head()


# In[17]:


# 划分训练集和测试集 
x_train,x_val,y_train,y_val=train_test_split(X.values,Y,test_size=.20,random_state=2)


# ## 构建模型

# ### 线性回归模型

# In[18]:


from sklearn.linear_model import LinearRegression
# 训练
reg = LinearRegression().fit(x_train, y_train)
print(reg.score(x_train,y_train))
# 使用模型进行行程时长预测（单位：分钟）
predictions = reg.predict(x_val)


# In[19]:


# 计算均方误差
from sklearn.metrics import mean_squared_error
dec_mse = mean_squared_error(predictions, y_val)
rmse = np.sqrt(dec_mse)
print(rmse) 


# ### 随机森林回归模型

# In[26]:


# 随机参数优化搜索最优参数
from sklearn.ensemble import RandomForestRegressor
from scipy.stats import randint as sp_randint
from sklearn.model_selection import RandomizedSearchCV
clf = RandomForestRegressor(random_state =42)
param_grid = {"max_depth": [10,20,30,40,50,60,70,80],
              "max_features": sp_randint(1, 15),
              "min_samples_split": sp_randint(2, 16),
              "min_samples_leaf": sp_randint(1, 16)}

validator = RandomizedSearchCV(clf, param_distributions= param_grid) 
validator.fit(x_train,y_train)
print(validator.best_score_)
print(validator.best_estimator_.n_estimators)
print(validator.best_estimator_.max_depth)
print(validator.best_estimator_.min_samples_split)
print(validator.best_estimator_.min_samples_leaf)
print(validator.best_estimator_.max_features)


# In[51]:


# 设置模型参数
rf_model = RandomForestRegressor(max_depth = 20 , min_samples_split=7, 
                                 min_samples_leaf=4, n_estimators =10, max_features =13)
# 训练
rf_model.fit(x_train,y_train)


# In[52]:


# 使用模型进行行程时长预测（单位：分钟）
predictions = rf_model.predict(x_val)


# In[53]:


# 计算均方误差
from sklearn.metrics import mean_squared_error
dec_mse = mean_squared_error(predictions, y_val)
rmse = np.sqrt(dec_mse)
print(rmse) 


# In[54]:


predictions[:10]


# In[55]:


y_val[:10]


# ### XGBoost模型

# In[18]:


import xgboost as xgb
from sklearn.externals import joblib

dtrain = xgb.DMatrix(x_train, label=y_train)
dvalid = xgb.DMatrix(x_val, label=y_val)
watchlist = [(dtrain, 'train'), (dvalid, 'valid')]


# In[19]:


# 随机参数优化搜索最优参数
from sklearn import datasets
from sklearn.linear_model import Ridge
from sklearn.ensemble import RandomForestRegressor
from scipy.stats import randint as sp_randint
from sklearn.model_selection import RandomizedSearchCV
clf = xgb.XGBRegressor()
param_grid = {"max_depth": [6,10],
              "min_child_weight": sp_randint(1, 11),
              "reg_lambda": sp_randint(1,3),
              "learning_rate": [0.05,0.01,0.03]}

validator = RandomizedSearchCV(clf, param_distributions= param_grid) 
validator.fit(x_train,y_train)
print(validator.best_score_)
print(validator.best_estimator_.max_depth)
print(validator.best_estimator_.min_child_weight)
print(validator.best_estimator_.reg_lambda)
print(validator.best_estimator_.subsample)
print(validator.best_estimator_.learning_rate)


# In[20]:


# 参数设置
xgb_pars = {'min_child_weight': 7, 'eta': 0.05, 'colsample_bytree': 0.9, 
            'max_depth':6,
            
'subsample': 1., 'lambda': 2., 'nthread': 4, 'booster' : 'gbtree', 'silent': 1,
'eval_metric': 'rmse', 'objective': 'reg:linear'}


# In[21]:


# 训练
model = xgb.train(xgb_pars, dtrain, 10000, watchlist, early_stopping_rounds=100,
                  maximize=False, verbose_eval=100)


# In[22]:



xgb_model_path = "./xgb_trip_duration.model"
#保存训练好的xgboost模型
joblib.dump(model, xgb_model_path)


# In[23]:


# 加载训练好的xgboost模型
model = joblib.load(xgb_model_path)


# In[24]:


# 使用模型进行行程时长预测（单位：分钟）
predictions = model.predict(dvalid)


# In[67]:


# 查看预测值与真实值
pd_y_pred = pd.DataFrame(predictions, columns = ['y_pred'])
pd_y_val= pd.DataFrame(y_val, columns = ['y_val'])
result = pd.concat([pd_y_pred, pd_y_val], axis=1)
result.head(20)

